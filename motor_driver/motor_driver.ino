#include <Servo.h> 

#define TRIGGER_PIN 12
#define SLOW_SERVO_PIN 9
#define FAST_SERVO_PIN 5

Servo slowServo;
Servo fastServo;

int range = 20;
int startPos = 45;
int minPos = startPos - range;
int maxPos = startPos + range;

int slowPos = startPos;
int slowDelta = 1;
int fastPos = startPos;
int fastDelta = 5;

int on = 0;
int triggerState = 0;

void blink(int count) {
  for (int i = 0; i < count; i++) {
    delay(250);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(250);
    digitalWrite(LED_BUILTIN, LOW);
  }
}

void setup() {
  Serial.begin(9600);
  Serial.println("Starting");
  slowServo.attach(SLOW_SERVO_PIN);
  slowServo.write(startPos);
  fastServo.attach(FAST_SERVO_PIN);
  fastServo.write(startPos);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  pinMode(TRIGGER_PIN, INPUT);
  delay(2000);
  blink(3);
  Serial.println("Ready");
}

void loop() {
  triggerState = digitalRead(TRIGGER_PIN);
  if (triggerState == LOW) {
    if (on == 1) {
      Serial.println("Turning off");
      digitalWrite(LED_BUILTIN, LOW);
    }
    on = 0;
  } else {
    if (on == 0) {
      digitalWrite(LED_BUILTIN, HIGH);
      Serial.println("Turning on!");
    }
    on = 1;
  }
  if (on) {
    slowPos = slowPos + slowDelta;
    slowServo.write(slowPos);
  
    fastPos = maxPos - slowPos + fastDelta + 25;
    fastDelta = fastDelta * -1;
    fastServo.write(fastPos);
    if (slowPos > maxPos || slowPos <= minPos) {
      slowDelta = slowDelta * -1;
    }
    delay(15);
  }
}
